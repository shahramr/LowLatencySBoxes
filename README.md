# Low-Latency Boolean Function and Bijective S-boxes

This repository contains the Boolean functions and S-boxes described or found in the paper "Low-Latency Boolean Function and Bijective S-boxes", ToSC Volume 2022, Issue 3.

On 1st April 2023, we correct the typos for the case of 5-bit functions. The updated version of paper is added to the repository.
